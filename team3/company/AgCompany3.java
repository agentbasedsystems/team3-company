package org.xploration.team3.company;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.xploration.ontology.RegistrationRequest;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;

import static jade.lang.acl.MessageTemplate.*;

public class AgCompany3 extends Agent {

    private static final long serialVersionUID = -3989737101882368246L;

    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private final int teamId = 3;

    @Override
    public void setup() {
        System.out.println(getLocalName() + ":\t\thas entered into the system");
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        addBehaviour(addRegisterCompanyBehaviour());
    }

    private Behaviour addRegisterCompanyBehaviour() {

        return new OneShotBehaviour(this) {

            @Override
            public void action() {
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.REGISTRATIONDESK);
                dfd.addServices(sd);
                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = DFService.search(myAgent, dfd);
                    while (res.length < 1) {
                        res = DFService.search(myAgent, dfd);
                        if (res.length < 1) doWait(500); // no regDesk found, wait a while and try again
                    }
                    // Gets the first occurrence, if there was success

                    AID registrationDeskAgent = res[0].getName();
                    ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                    msg.addReceiver(registrationDeskAgent);
                    msg.setLanguage(codec.getName());
                    msg.setOntology(ontology.getName());
                    msg.setProtocol(XplorationOntology.REGISTRATIONREQUEST);
                    // RegistrationRequest regRequest = new
                    // DefaultRegistrationRequest();
                    // Team ownTeam = new DefaultTeam();
                    RegistrationRequest regRequest = new RegistrationRequest();
                    Team myTeam = new Team();
                    myTeam.setTeamId(teamId);
                    regRequest.setTeam(myTeam);
                    // As it is an action and the encoding language the SL,
                    // it must be wrapped
                    // into an Action
                    Action agAction = new Action(registrationDeskAgent, regRequest);
                    try {
                        // The ContentManager transforms the java objects
                        // into strings
                        getContentManager().fillContent(msg, agAction);
                        send(msg);
                        System.out.println(getLocalName() + ":\t\tsent a REGISTRATION " + ACLMessage.getPerformative(msg.getPerformative()) + " to " + registrationDeskAgent.getLocalName());
                        ACLMessage rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                MessageTemplate.and(MatchOntology(ontology.getName()),
                                        MatchProtocol(XplorationOntology.REGISTRATIONREQUEST))));
                        switch (rcv.getPerformative()) {
                            case ACLMessage.REFUSE:
                                System.out.println(myAgent.getLocalName() + ":\t\t received a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                System.out.printf(myAgent.getLocalName() + ":\t\tkilling self");
                                break;
                            case ACLMessage.NOT_UNDERSTOOD:
                                System.out.println(myAgent.getLocalName() + ":\t\t received a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                System.out.printf(myAgent.getLocalName() + ":\t\tkilling self");
                                break;
                            case ACLMessage.AGREE:
                                rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                        MessageTemplate.and(MatchOntology(ontology.getName()),
                                                MatchProtocol(XplorationOntology.REGISTRATIONREQUEST))));
                                switch (rcv.getPerformative()) {
                                    case ACLMessage.FAILURE:
                                        System.out.println(myAgent.getLocalName() + ":\t\t received a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                        System.out.printf(myAgent.getLocalName() + ":\t\tkilling self");
                                        break;
                                    case ACLMessage.INFORM:
                                        System.out.println(myAgent.getLocalName() + ":\t\t received a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                        break;
                                    default:
                                        System.out.println(myAgent.getLocalName() + ":\t\t received a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                        System.out.printf(myAgent.getLocalName() + ":\t\tkilling self");
                                        break;
                                }
                                break;
                            default:
                                System.out.println(myAgent.getLocalName() + ":\t\treceived a " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + rcv.getSender().getLocalName());
                                System.out.printf(myAgent.getLocalName() + ":\t\tkilling self");
                                break;
                        }

                    } catch (CodecException | OntologyException e) {
                        //e.printStackTrace();
                        //doWait(10000);
                    }

                } catch (Exception e) {
                   // e.printStackTrace();
                }
            }

            @Override
            public int onEnd() {
                System.out.println(myAgent.getLocalName() + ":\t\t is no longer useful, killing self");
                doDelete(); // kill self
                return super.onEnd();
            }
        };
    }



}
