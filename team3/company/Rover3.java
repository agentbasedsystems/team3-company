package org.xploration.team3.company;


import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.xploration.ontology.*;

import java.util.ArrayList;

import static jade.lang.acl.MessageTemplate.*;

import java.util.Collections;

public class Rover3 extends Agent {

    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();

    private Team myTeam;

    private ArrayList<Cell> movementPath;
    private ArrayList<Cell> savedPath;
    private boolean goToCapsule = false;
    private int circleRange = 0; // the circle range (everytime getting bigger)
    private int nbrCellToAnalyze; // number of cells to analyze before going home (add the time variable)
    private Cell currentCell;
    private org.xploration.ontology.Map claimQueue=new org.xploration.ontology.Map();
    private org.xploration.ontology.Map mapToBroadCast=new org.xploration.ontology.Map();
    private Map map;
    private int numberOfNewUnclaimedCells;
    private Cell capsuleLocation;

    private AID terrainSimulatorID, roverRegistrationServiceID, mapBroadcastServiceID, movementRequestServiceID, communicationServiceID;
    private boolean foundSomething = false;

    private ThreadedBehaviourFactory tbf;

    private int missionLength;
    private int communicationRange;
    private int analyzingTime=-1,movingTime=-1;

    @Override
    protected void setup() {
        super.setup();

        System.out.println(getLocalName() + ":\t\thas entered into the system");
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        movementPath = new ArrayList<>();
        savedPath = new ArrayList<>();

        myTeam = new Team();
        myTeam.setTeamId(3);

        Object[] args = getArguments();
        int x = (int) args[0];
        int y = (int) args[1];
        int x_dim = (int) args[2];
        int y_dim = (int) args[3];
        missionLength = (int) args[4];
        missionLength*=1000;
        communicationRange = (int) args[5];

        nbrCellToAnalyze = missionLength/(6*(1000+1000));
        //System.err.println("CELLTOANALYZE: " + nbrCellToAnalyze);

        numberOfNewUnclaimedCells = 0;

        currentCell = new Cell();
        currentCell.setX(x);
        currentCell.setY(y);

        capsuleLocation = new Cell();
        capsuleLocation.setX(x);
        capsuleLocation.setY(y);
        try {
            map = new Map(x_dim, y_dim, communicationRange);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tbf = new ThreadedBehaviourFactory();
        addBehaviour(findAllAIDBehaviour());
        addBehaviour(tbf.wrap(killSelfBehaviour(missionLength)));
    }

    private Behaviour killSelfBehaviour(int missionLength) {
        return new WakerBehaviour(this, missionLength) {
            @Override
            protected void onWake() {
                super.onWake();
                System.out.println(myAgent.getLocalName() + ": reached end of mission, KILLING self");
            }

            @Override
            public int onEnd() {
                doDelete();
                return super.onEnd();
            }
        };
    }

    private Behaviour findAllAIDBehaviour() {
        return new SimpleBehaviour(this) {
            void findAID(String serviceName) {
                ServiceDescription sd = new ServiceDescription();
                DFAgentDescription dfd = new DFAgentDescription();
                sd.setType(serviceName);
                dfd.addServices(sd);

                try {
                    DFAgentDescription[] res = DFService.search(myAgent, dfd);
                    if (res.length > 0) {
                        switch (serviceName) {
                            case XplorationOntology.RADIOCLAIMSERVICE:
                                communicationServiceID = res[0].getName();
                                addBehaviour(addClaimCellInfoBehaviour());
                                break;
                            case XplorationOntology.TERRAINSIMULATOR:
                                terrainSimulatorID = res[0].getName();
                                break;
                            case XplorationOntology.ROVERREGISTRATIONSERVICE:
                                roverRegistrationServiceID = res[0].getName();
                                addBehaviour(addRoverRegistrationBehaviour());
                                break;
                            case XplorationOntology.MOVEMENTREQUESTSERVICE:
                                movementRequestServiceID = res[0].getName();
                                addBehaviour(addMovementBehaviour());
                                break;
                            case XplorationOntology.MAPBROADCASTSERVICE:
                                mapBroadcastServiceID = res[0].getName();
                                //addBehaviour(tbf.wrap(addMapBroadcastBehaviour()));
                                addBehaviour(tbf.wrap(addGetMapBroadcastBehaviour()));
                                break;
                        }
                    }
                } catch (FIPAException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void action() {
                if (roverRegistrationServiceID == null) findAID(XplorationOntology.ROVERREGISTRATIONSERVICE);
                if (terrainSimulatorID == null) findAID(XplorationOntology.TERRAINSIMULATOR);
                if (mapBroadcastServiceID == null) findAID(XplorationOntology.MAPBROADCASTSERVICE);
                if (movementRequestServiceID == null) findAID(XplorationOntology.MOVEMENTREQUESTSERVICE);
                if (communicationServiceID == null) findAID(XplorationOntology.RADIOCLAIMSERVICE);
            }

            @Override
            public boolean done() {
                return terrainSimulatorID != null &&
                        roverRegistrationServiceID != null &&
                        mapBroadcastServiceID != null &&
                        communicationServiceID != null &&
                        movementRequestServiceID != null;
            }
        };
    }

    private Behaviour addClaimCellInfoBehaviour() {
        return new CyclicBehaviour(this) {
            @Override
            public void action() {
                if (getDistance(currentCell, capsuleLocation) <= communicationRange && numberOfNewUnclaimedCells > 0) {
                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                    msg.addReceiver(communicationServiceID);
                    msg.setLanguage(codec.getName());
                    msg.setOntology(ontology.getName());
                    msg.setProtocol(XplorationOntology.CLAIMCELLINFO);
                    ClaimCellInfo cci = new ClaimCellInfo();
                    cci.setMap(claimQueue);
                    cci.setTeam(myTeam);
                    AgentAction agentAction = new Action(communicationServiceID, cci);

                    try {
                        getContentManager().fillContent(msg, agentAction);
                        send(msg);
                        System.out.println(getLocalName() + ":\t\tsent CLAIMCELLINFO " + ACLMessage.getPerformative(msg.getPerformative()) + " to " + communicationServiceID.getLocalName());
                        mapToBroadCast.setCellList(claimQueue.getCellList());
                        claimQueue=new org.xploration.ontology.Map();
                        numberOfNewUnclaimedCells = 0;
                    } catch (Codec.CodecException | OntologyException e) {
                        //e.printStackTrace();
                        //doWait(5000);
                    }
                }
            }


        };
    }

    private Behaviour addMovementBehaviour() {
        return new SimpleBehaviour(this) {
            @Override
            public void action() {
                if (movementRequestServiceID != null) {
                    Cell toCell = decideMovement();
                    ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                    msg.addReceiver(movementRequestServiceID);
                    msg.setLanguage(codec.getName());
                    msg.setOntology(ontology.getName());
                    msg.setProtocol(XplorationOntology.MOVEMENTREQUESTINFO);
                    MovementRequestInfo mri = new MovementRequestInfo();
                    mri.setCell(toCell);
                    mri.setTeam(myTeam);
                    AgentAction agAction = new Action(movementRequestServiceID, mri);

                    try {
                        getContentManager().fillContent(msg, agAction);
                        send(msg);
                        System.out.println(getLocalName() + ":\t\tsent MOVEMENTREQUEST request to cell [" + toCell.getX() + "," + toCell.getY() + "] from [" + currentCell.getX() + "," + currentCell.getY() + "] to " + movementRequestServiceID.getLocalName());
                        ACLMessage rcv = null;

                        rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                MessageTemplate.and(MatchOntology(ontology.getName()),
                                        MessageTemplate.and(MatchSender(movementRequestServiceID),
                                                MatchProtocol(XplorationOntology.MOVEMENTREQUESTINFO)))));
                        if (rcv != null) {
                            AID fromAgent = rcv.getSender();
                            switch (rcv.getPerformative()) {
                                case ACLMessage.REFUSE:
                                    System.out.println(myAgent.getLocalName() + ":\t\treceived MOVEMENTREQUEST " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + fromAgent.getLocalName());
                                    // invalid cell!
                                    // throw exception?
                                    break;
                                case ACLMessage.NOT_UNDERSTOOD:
                                    System.out.println(myAgent.getLocalName() + ":\t\treceived MOVEMENTREQUEST " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + fromAgent.getLocalName());
                                    throw new NotUnderstoodException(myAgent.getLocalName() + " received a NOT_UNDERSTOOD message from " + fromAgent.getLocalName());
                                case ACLMessage.AGREE:
                                    System.out.println(myAgent.getLocalName() + ":\t\treceived MOVEMENTREQUEST " + ACLMessage.getPerformative(rcv.getPerformative()) + " from " + fromAgent.getLocalName());
                                    // wait for INFORM or FAILURE
                                    rcv = null;
                                    long start=0L;
                                    long stop=0L;
                                    if(movingTime==-1){
                                        start=System.currentTimeMillis();
                                    }
                                    while (rcv == null) {
                                        rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                                MessageTemplate.and(MatchOntology(ontology.getName()),
                                                        MessageTemplate.and(MatchSender(movementRequestServiceID),
                                                                MatchProtocol(XplorationOntology.MOVEMENTREQUESTINFO)))));
                                    }
                                    if(movingTime==-1){
                                        movingTime=(int)(System.currentTimeMillis()-start);
                                    }
                                    switch (rcv.getPerformative()) {
                                        case ACLMessage.FAILURE:
                                            // illegal request
                                            // TODO: what to do?
                                            System.out.println(getLocalName() + ":\t\treceived a MOVEMENTREQUEST FAILURE from " + rcv.getSender().getLocalName());
                                            break;
                                        case ACLMessage.INFORM:
                                            System.out.println(getLocalName() + ":\t\treceived a MOVEMENTREQUEST INFORM from " + rcv.getSender().getLocalName());
                                            currentCell.setX(toCell.getX());
                                            currentCell.setY(toCell.getY());
                                            System.out.println(myAgent.getLocalName() + ":\t\tmoved to [" + toCell.getX() + "," + toCell.getY() + "]");
                                            //addBehaviour(addAnalyzeCellBehaviour());
                                            try {
                                                // only analyze if not yet analyzed or received by other rover
                                                if (map.getCell(currentCell.getX(), currentCell.getY()).getMineral() == null || map.getCell(currentCell.getX(), currentCell.getY()).getMineral().equalsIgnoreCase("")) {
                                                    //addBehaviour(analyzeCellBehaviour());
                                                    analyzeCell(myAgent);
                                                }
                                            } catch (MapException e) {
                                                e.printStackTrace();
                                            }
                                            broadcastMap();
                                            break;
                                        default:
                                            // weird message received!
                                            // reply NOT_UNDERSTOOD
                                            ACLMessage reply = rcv.createReply();
                                            reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                                            myAgent.send(reply);
                                            System.out.println(myAgent.getLocalName() + ":\t\treceived a message it didn't understand from " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                                            break;
                                    }
                                    break;
                                default:
                                    // weird message received!
                                    // reply NOT_UNDERSTOOD
                                    ACLMessage reply = rcv.createReply();
                                    reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName() + ":\t\treceived a message it didn't understand from " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                                    break;
                            }
                        } else {
                            block();
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        //e.printStackTrace();
                        //doWait(5000);
                    } catch (NotUnderstoodException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public boolean done() {
                return false;
            }
        };
    }

    // We follow movementPath 
        private Cell decideMovement() {
        Cell nextCell;

        // if we finish our journey we find a new path
        if (movementPath.isEmpty()) {
            if (goToCapsule) { // we are in capsule range
                nbrCellToAnalyze = missionLength/(6*(movingTime+analyzingTime)); // new Cell counter before going back
                movementPath = pathToCell(currentCell, savedPath.get(0), 1);
                movementPath.addAll(savedPath);
                goToCapsule = false;
            } else { // if we finished our circle and still don't need to go to our capsule
                circleRange++; // make the range bigger
                movementPath = orderPath(map.getRing(capsuleLocation, circleRange)); // and we find a new path
                if (movementPath.isEmpty()) {
                    circleRange = 1;
                    movementPath = orderPath(map.getRing(capsuleLocation, circleRange));
                }
            }
        }

        if (nbrCellToAnalyze <= 0 && goToCapsule != true && getDistance(currentCell, capsuleLocation) > communicationRange) { // get back to capsule
            savedPath = movementPath; // we save our progress in the ring
            movementPath = pathToCell(currentCell, capsuleLocation, communicationRange);
            goToCapsule = true;
        }

        nextCell = movementPath.get(0);
        movementPath.remove(0);

        return nextCell;
    }

    public ArrayList<Cell> orderPath(ArrayList<Cell> toOrder) {
        int shortestDistance;
        Cell closestCell;
        ArrayList<Cell> orderedPath = new ArrayList<>();
        orderedPath.add(currentCell);
        while (!toOrder.isEmpty()) {
            shortestDistance = getDistance(orderedPath.get(orderedPath.size()-1), toOrder.get(0));
            closestCell = toOrder.get(0);
            for (int j = 0; j < toOrder.size(); j++) {
                if (getDistance(orderedPath.get(orderedPath.size()-1), toOrder.get(j)) < shortestDistance) {
                    shortestDistance = getDistance(orderedPath.get(orderedPath.size()-1), toOrder.get(j));
                    closestCell = toOrder.get(j);
                }

            }
            if (shortestDistance == 1) {
                orderedPath.add(closestCell);
            } else {
                orderedPath.addAll(pathToCell(orderedPath.get(orderedPath.size()-1), closestCell, 0));
            }
            toOrder.remove(closestCell);
        }
        orderedPath.remove(0);
        return orderedPath;
    }

    // get the closest path to get to the closestGoodCell
    // and saves the path in movementPath

    private ArrayList<Cell> pathToCell(Cell from, Cell to, int closeness) {
        ArrayList<Cell> path = new ArrayList<>();
        Cell closest = from;
        for (int i = 0; i < getDistance(from, to)-closeness; i++) {
            ArrayList<Cell> temp = new ArrayList<>();
            for (Cell c : map.getNeighbours(closest)) {
                if (map.getDistance(c, to) < map.getDistance(closest, to)) {
                    closest = c;
                    break;
                }
            }
            path.add(closest);
        }
        return path;
    }

    private Behaviour addRoverRegistrationBehaviour() {
        return new OneShotBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                msg.addReceiver(roverRegistrationServiceID);
                msg.setLanguage(codec.getName());
                msg.setOntology(ontology.getName());
                msg.setProtocol(XplorationOntology.ROVERREGISTRATIONINFO);
                RoverRegistrationInfo roverRegistrationInfo = new RoverRegistrationInfo();
                roverRegistrationInfo.setCell(currentCell);
                roverRegistrationInfo.setTeam(myTeam);
                Action agAction = new Action(roverRegistrationServiceID, roverRegistrationInfo);
                try {
                    getContentManager().fillContent(msg, agAction);
                    send(msg);
                    System.out.println(getLocalName() + ":\t\tsent ROVERREGISTRATIONINFO " + ACLMessage.getPerformative(msg.getPerformative()) + " to " + roverRegistrationServiceID.getLocalName() + "\tlocation: [" + currentCell.getX() + "," + currentCell.getY() + "]");
                } catch (Codec.CodecException | OntologyException e) {
                    e.printStackTrace();
                    System.out.println(getLocalName() + ":\t\terror in roverRegistration");
                }
            }
        };
    }

    private void broadcastMap() {
        if (foundSomething && mapToBroadCast.getCellList().size()>0) {
            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.addReceiver(mapBroadcastServiceID);
            msg.setLanguage(codec.getName());
            msg.setOntology(ontology.getName());
            msg.setProtocol(XplorationOntology.MAPBROADCASTINFO);
            MapBroadcastInfo mapBroadcastInfo = new MapBroadcastInfo();
            mapBroadcastInfo.setMap(mapToBroadCast); // give map
            Action agAction = new Action(mapBroadcastServiceID, mapBroadcastInfo);
            try {
                getContentManager().fillContent(msg, agAction);
                send(msg);
                System.out.println(getLocalName() + ":\t\tsent MAPBROADCASTINFO INFORM to " + mapBroadcastServiceID.getLocalName());
            } catch (Codec.CodecException | OntologyException e) {
                //e.printStackTrace();
                System.out.println(getLocalName() + ":\t\terror in mapBroadcast");
                //doWait(5000);
            }
        }
    }

    private Behaviour addGetMapBroadcastBehaviour() {
        return new CyclicBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = receive(MessageTemplate.and(MatchLanguage(codec.getName()),
                        MessageTemplate.and(MatchOntology(ontology.getName()),
                                MessageTemplate.and(MatchSender(mapBroadcastServiceID),
                                        MatchProtocol(XplorationOntology.MAPBROADCASTINFO)))));
                if (msg != null) {
                    if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
                        System.out.println(myAgent.getLocalName() + ":\t\treceived a MAPBROADCASTINFO NOT_UNDERSTOOD from " + msg.getSender().getLocalName());
                    } else if (msg.getPerformative() == ACLMessage.INFORM) {
                        try {
                            ContentElement ce = getContentManager().extractContent(msg);
                            if (ce instanceof Action) {
                                Action agAction = (Action) ce;
                                Concept concept = agAction.getAction();
                                if (concept instanceof MapBroadcastInfo) {
                                    System.out.println(myAgent.getLocalName() + ":\t\treceived a MAPBROADCASTINFO inform from " + msg.getSender().getLocalName());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else // no message
                    block();
            }
        };
    }

    private Behaviour analyzeCellBehaviour() {
        return new OneShotBehaviour() {
            @Override
            public void action() {
                analyzeCell(myAgent);
            }

            private void analyzeCell(Agent myAgent) {
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.addReceiver(terrainSimulatorID);
                msg.setLanguage(codec.getName());
                msg.setOntology(ontology.getName());
                msg.setProtocol(XplorationOntology.CELLANALYSIS);
                CellAnalysis cellAnalysis = new CellAnalysis();
                cellAnalysis.setCell(currentCell);
                Action agAction = new Action(terrainSimulatorID, cellAnalysis);
                try {
                    getContentManager().fillContent(msg, agAction);
                    send(msg);
                    System.out.println(getLocalName() + ":\t\tsent CELLANALYSIS REQUEST of cell [" + currentCell.getX() + "," + currentCell.getY() + "] to " + terrainSimulatorID.getLocalName());
                    // REQUEST SENT
                    // WAITING FOR RESPONSE

                    // is this boolean expression correct?
                    ACLMessage rcv = null;

                    rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                            MessageTemplate.and(MatchOntology(ontology.getName()),
                                    MessageTemplate.and(MatchSender(terrainSimulatorID),
                                            MatchProtocol(XplorationOntology.CELLANALYSIS)))));

                    if (rcv != null) {
                        AID fromAgent = rcv.getSender();
                        System.out.println(myAgent.getLocalName() + ":\t\treceived response to CELLANALYSIS request from " + fromAgent.getLocalName());

                        switch (rcv.getPerformative()) {
                            case ACLMessage.REFUSE:
                                // invalid cell!
                                // throw exception?
                                break;
                            case ACLMessage.NOT_UNDERSTOOD:
                                System.out.println(myAgent.getLocalName() + ":*******received NOT_UNDERSTOOD from " + fromAgent.getLocalName());
                                throw new NotUnderstoodException(myAgent.getLocalName() + " received a NOT_UNDERSTOOD from" + fromAgent.getLocalName());
                            case ACLMessage.AGREE:
                                // wait for INFORM or FAILURE
                                rcv = null;
                                while (rcv == null) {
                                    rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                            MessageTemplate.and(MatchOntology(ontology.getName()),
                                                    MessageTemplate.and(MatchSender(terrainSimulatorID),
                                                            MatchProtocol(XplorationOntology.CELLANALYSIS)))));
                                }
                                ContentElement ce = getContentManager().extractContent(rcv);
                                if (ce instanceof Action) {
                                    Concept concept = ((Action) ce).getAction();
                                    if (concept instanceof CellAnalysis) {
                                        fromAgent = rcv.getSender();
                                        System.out.println(myAgent.getLocalName() + ":\t\treceived second response to CELLANALYSIS request from " + fromAgent.getLocalName());

                                        switch (rcv.getPerformative()) {
                                            case ACLMessage.FAILURE:
                                                // illegal request
                                                // TODO: what to do?
                                                System.out.println(getLocalName() + ":\t\treceived CELLANALYSIS FAILURE from " + fromAgent.getLocalName());
                                                break;
                                            case ACLMessage.INFORM:
                                                Cell repliedCell = ((CellAnalysis) concept).getCell();
                                                try {
                                                    nbrCellToAnalyze--;
                                                    map.putMineral(repliedCell.getX(), repliedCell.getY(), repliedCell.getMineral());
                                                    foundSomething = true;
                                                    numberOfNewUnclaimedCells++;

                                                } catch (Exception e) {
                                                    // shouldnt happen
                                                }
                                                System.out.println(myAgent.getLocalName() + " received answer that cell [" + repliedCell.getX() + "," + repliedCell.getY() + "] contains mineral " + repliedCell.getMineral() + " from " + fromAgent.getLocalName());
                                                break;
                                            default:
                                                // weird message received!
                                                // reply NOT_UNDERSTOOD
                                                ACLMessage reply = rcv.createReply();
                                                reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                                                myAgent.send(reply);

                                                System.out.println(myAgent.getLocalName() + ":\t\treceived message it didn't understand " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                                                break;
                                        }

                                    } else {
                                        // second content not instanceof CellAnalysis
                                    }
                                } else {
                                    // second ce not instanceof Action
                                }

                                break;
                            default:
                                // weird message received!
                                // reply NOT_UNDERSTOOD
                                ACLMessage reply = rcv.createReply();

                                reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);

                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName() + ":\t\treceived message it didn't understand " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                                break;
                        }
                    } else {
                        block();
                    }
                } catch (Codec.CodecException | OntologyException | NotUnderstoodException e) {
                    //e.printStackTrace();

                    //doWait(5000);
                }
            }

        };
    }


    private void analyzeCell(Agent myAgent) {
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(terrainSimulatorID);
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        msg.setProtocol(XplorationOntology.CELLANALYSIS);
        CellAnalysis cellAnalysis = new CellAnalysis();
        cellAnalysis.setCell(currentCell);
        Action agAction = new Action(terrainSimulatorID, cellAnalysis);
        try {
            getContentManager().fillContent(msg, agAction);
            send(msg);
            System.out.println(getLocalName() + ":\t\tsent CELLANALYSIS REQUEST of cell [" + currentCell.getX() + "," + currentCell.getY() + "] to " + terrainSimulatorID.getLocalName());
            // REQUEST SENT
            // WAITING FOR RESPONSE

            // is this boolean expression correct?
            ACLMessage rcv = null;

            rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                    MessageTemplate.and(MatchOntology(ontology.getName()),
                            MessageTemplate.and(MatchSender(terrainSimulatorID),
                                    MatchProtocol(XplorationOntology.CELLANALYSIS)))));

            if (rcv != null) {
                AID fromAgent = rcv.getSender();
                System.out.println(myAgent.getLocalName() + ":\t\treceived response to CELLANALYSIS request from " + fromAgent.getLocalName());

                switch (rcv.getPerformative()) {
                    case ACLMessage.REFUSE:
                        // invalid cell!
                        // throw exception?
                        break;
                    case ACLMessage.NOT_UNDERSTOOD:
                        System.out.println(myAgent.getLocalName() + ":*******received NOT_UNDERSTOOD from " + fromAgent.getLocalName());
                        throw new NotUnderstoodException(myAgent.getLocalName() + " received a NOT_UNDERSTOOD from" + fromAgent.getLocalName());
                    case ACLMessage.AGREE:
                        // wait for INFORM or FAILURE
                        rcv = null;
                        long start=0;
                        long stop=0;
                        if(analyzingTime==-1){
                            start=System.currentTimeMillis();
                        }
                        while (rcv == null) {
                            rcv = blockingReceive(MessageTemplate.and(MatchLanguage(codec.getName()),
                                    MessageTemplate.and(MatchOntology(ontology.getName()),
                                            MessageTemplate.and(MatchSender(terrainSimulatorID),
                                                    MatchProtocol(XplorationOntology.CELLANALYSIS)))));
                        }
                        if(analyzingTime==-1){
                            analyzingTime=(int)(System.currentTimeMillis()-start);
                            nbrCellToAnalyze = missionLength/(6*(movingTime+analyzingTime));
                        }
                        ContentElement ce = getContentManager().extractContent(rcv);
                        if (ce instanceof Action) {
                            Concept concept = ((Action) ce).getAction();
                            if (concept instanceof CellAnalysis) {
                                fromAgent = rcv.getSender();
                                System.out.println(myAgent.getLocalName() + ":\t\treceived second response to CELLANALYSIS request from " + fromAgent.getLocalName());

                                switch (rcv.getPerformative()) {
                                    case ACLMessage.FAILURE:
                                        // illegal request
                                        // TODO: what to do?
                                        System.out.println(getLocalName() + ":\t\treceived CELLANALYSIS FAILURE from " + fromAgent.getLocalName());
                                        break;
                                    case ACLMessage.INFORM:
                                        Cell repliedCell = ((CellAnalysis) concept).getCell();
                                        try {
                                            nbrCellToAnalyze--;
                                            map.putMineral(repliedCell.getX(), repliedCell.getY(), repliedCell.getMineral());
                                            foundSomething = true;
                                            numberOfNewUnclaimedCells++;
                                            claimQueue.addCellList(repliedCell);
                                        } catch (Exception e) {
                                            // shouldnt happen
                                        }
                                        System.out.println(myAgent.getLocalName() + " received answer that cell [" + repliedCell.getX() + "," + repliedCell.getY() + "] contains mineral " + repliedCell.getMineral() + " from " + fromAgent.getLocalName());
                                        break;
                                    default:
                                        // weird message received!
                                        // reply NOT_UNDERSTOOD
                                        ACLMessage reply = rcv.createReply();
                                        reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                                        myAgent.send(reply);

                                        System.out.println(myAgent.getLocalName() + ":\t\treceived message it didn't understand " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                                        break;
                                }

                            } else {
                                // second content not instanceof CellAnalysis
                            }
                        } else {
                            // second ce not instanceof Action
                        }

                        break;
                    default:
                        // weird message received!
                        // reply NOT_UNDERSTOOD
                        ACLMessage reply = rcv.createReply();

                        reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);

                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName() + ":\t\treceived message it didn't understand " + fromAgent.getLocalName() + " and sent NOT_UNDERSTOOD");
                        break;
                }
            } else {

            }
        } catch (Codec.CodecException | OntologyException | NotUnderstoodException e) {
            //e.printStackTrace();

            //doWait(5000);
        }
    }

    public int getDistance(Cell a, Cell b) {
        int rightDiff = (map.getWidth() + b.getY() - a.getY()) % map.getWidth();
        int leftDiff = (map.getWidth() + a.getY() - b.getY()) % map.getWidth();
        int upDiff = (map.getHeight() + a.getX() - b.getX()) % map.getHeight();
        int downDiff = (map.getHeight() + b.getX() - a.getX()) % map.getHeight();

        int distY = Math.min(rightDiff, leftDiff);
        int distX = Math.min(upDiff, downDiff);

        int distance = distY + Math.max(0, (distX - distY) / 2);
        return distance;
    }
}

