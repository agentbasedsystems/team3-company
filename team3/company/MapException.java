package org.xploration.team3.company;

public class MapException extends Exception{

    public MapException(String s) {
        super(s);
    }
}