package org.xploration.team3.company;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import org.xploration.ontology.*;

import static jade.lang.acl.MessageTemplate.MatchLanguage;
import static jade.lang.acl.MessageTemplate.MatchOntology;
import static jade.lang.acl.MessageTemplate.MatchReceiver;


public class Capsule3 extends Agent {

    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();

    private final int teamId = 3;
    private Cell myLocation;
    private Map map;
    private int missionLength;
    private int communicationRange;

    @Override
    protected void setup() {
        super.setup();

        System.out.println(getLocalName() + ":\t\thas entered into the system");
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        Object[] args = getArguments();

        int x = (int) args[0];
        int y = (int) args[1];
        int width = (int) args[2];
        int height = (int) args[3];
        missionLength = (int) args[4];
        communicationRange=(int) args[5];

        myLocation = new Cell();
        myLocation.setX(x);
        myLocation.setY(y);

        map = new Map(width, height,communicationRange);

        addBehaviour(addCapsuleRegistrationBehaviour());
        addBehaviour(addCreateRoverBehaviour());
        addBehaviour(addListenerBehaviour());
        addBehaviour(killSelfBehaviour(missionLength*1000));
    }

    private Behaviour addListenerBehaviour() {
        return new CyclicBehaviour(this) {
            @Override
            public void action() {
                ACLMessage msg = receive(MessageTemplate.and(MatchLanguage(codec.getName()),
                        MessageTemplate.and(MatchOntology(ontology.getName()),
                                MatchReceiver(new AID[]{myAgent.getAID()}))));
                if (msg != null) {
                    try {
                        ContentElement ce = getContentManager().extractContent(msg);
                        if (ce instanceof Action) {
                            Concept concept = ((Action) ce).getAction();
                            if (concept instanceof ClaimCellInfo) {
                                AID spacecraft = null;
                                DFAgentDescription dfd = new DFAgentDescription();
                                ServiceDescription sd = new ServiceDescription();
                                sd.setType(XplorationOntology.SPACECRAFTCLAIMSERVICE);
                                dfd.addServices(sd);
                                try {
                                    DFAgentDescription[] res = DFService.search(myAgent, dfd);
                                    if (res.length > 0) {
                                        spacecraft = res[0].getName();
                                    }
                                } catch (FIPAException e) {
                                   // e.printStackTrace();
                                }
                                if (spacecraft != null) {
                                    Team team3 = ((ClaimCellInfo) concept).getTeam();
                                    org.xploration.ontology.Map claimedMap = ((ClaimCellInfo) concept).getMap();
                                    ACLMessage finalClaimMsg = new ACLMessage(ACLMessage.INFORM);
                                    finalClaimMsg.addReceiver(spacecraft);
                                    finalClaimMsg.setLanguage(codec.getName());
                                    finalClaimMsg.setOntology(ontology.getName());
                                    finalClaimMsg.setProtocol(XplorationOntology.SPACECRAFTCLAIMSERVICE);
                                    ClaimCellInfo cci = new ClaimCellInfo();
                                    cci.setMap(claimedMap);
                                    cci.setTeam(team3);
                                    Action agentAction = new Action(spacecraft, cci);

                                    try {
                                        getContentManager().fillContent(finalClaimMsg, agentAction);
                                        send(finalClaimMsg);
                                        System.out.println(getLocalName() + ":\t\tsent CLAIMCELLINFO " + ACLMessage.getPerformative(msg.getPerformative()) + " to " + spacecraft.getLocalName());
                                    } catch (Codec.CodecException | OntologyException e) {
                                        //e.printStackTrace();
                                        //doWait(5000);
                                    }
                                }
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        //e.printStackTrace();
                    }
                } else {
                    block();
                }
            }
        };
    }

    private Behaviour addCreateRoverBehaviour() {

        return new OneShotBehaviour() {
            @Override
            public void action() {
                int x = myLocation.getX();
                int y = myLocation.getY();

                AgentContainer c = getContainerController();
                AgentController a;

                try {
                    Object[] args = new Object[]{x, y, map.getWidth(), map.getHeight(),missionLength,communicationRange};
                    a = c.createNewAgent("rover3", this.getClass().getPackage().getName() + ".Rover3", args);
                    a.start();
                } catch (StaleProxyException e) {
                   // e.printStackTrace();
                }
            }
        };

    }

    private Behaviour killSelfBehaviour(int missionLength) {
        return new WakerBehaviour(this, missionLength) {
            @Override
            protected void onWake() {
                super.onWake();
                System.out.println(myAgent.getLocalName()+": reached end of mission, KILLING self");
            }
            @Override
            public int onEnd() {
                doDelete();
                return super.onEnd();
            }
        };
    }

    private Behaviour addCapsuleRegistrationBehaviour() {
        return new OneShotBehaviour(this) {
            @Override
            public void action() {
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.CAPSULEREGISTRATIONSERVICE);
                dfd.addServices(sd);

                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    if (res.length > 0) {
                        AID simulatorAgent = res[0].getName();

                        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                        msg.addReceiver(simulatorAgent);
                        msg.setLanguage(codec.getName());
                        msg.setOntology(ontology.getName());
                        msg.setProtocol(XplorationOntology.CAPSULEREGISTRATIONINFO);
                        CapsuleRegistrationInfo cri = new CapsuleRegistrationInfo();
                        //LocationInform locInf = new LocationInform(); //need Ontology
                        Team ownTeam = new Team();
                        ownTeam.setTeamId(teamId);
                        cri.setTeam(ownTeam);
                        cri.setCell(myLocation);

                        Action agAction = new Action(simulatorAgent, cri);
                        try {
                            getContentManager().fillContent(msg, agAction);
                            send(msg);
                            System.out.println(getLocalName() + ":\t\tsent CAPSULE REGISTRATION INFO " + msg.getPerformative(msg.getPerformative()) + " to " + simulatorAgent.getLocalName() + "\tlocation: [" + myLocation.getX() + "," + myLocation.getY() + "]");
                        } catch (Codec.CodecException | OntologyException e) {
                            //e.printStackTrace();
                            //doWait(1000);
                        }
                    } else {
                        // If no RegistrationDesk has been found, wait 5 seconds
                        //doWait(1000);
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        };
    }


}
