package org.xploration.team3.company;

import org.xploration.ontology.Cell;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Map {

    Cell[][] map;
    private ArrayList<ArrayList<HashSet<Cell>>> neighbourlist;

    public Map(String path, int radioRange) {
        try {
            readMapFromFile(path);
            //initNeighbourlist(radioRange);
            //System.out.println("NEIGHBOUR:" + isInRange(map[5][5], map[3][7], 3));
            //System.out.println("****************************************************************");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Map(int x_dim, int y_dim, int radioRange) {
        map = new Cell[x_dim + 1][y_dim + 1];
        for (int i = 1; i <= x_dim; i++) {
            for (int j = 1; j <= y_dim; j++) {
                if (isValidPosition(i, j)) {
                    Cell c = new Cell();
                    c.setX(i);
                    c.setY(j);
                    map[i][j] = c;
                }
            }
        }
        //initNeighbourlist(radioRange);
    }

    private void initNeighbourlist(int radioRange) {
        neighbourlist = new ArrayList<>();
        try {
            for (int i = 0; i < map.length; ++i) {
                neighbourlist.add(new ArrayList<>());
                for (int j = 0; j < map[i].length; ++j) {
                    neighbourlist.get(i).add(new HashSet<>());
                    if (i % 2 == j % 2) {
                        if (i > 0 && j > 0) {
                            neighbourlist.get(i).get(j).addAll(getNeighbours(map[i][j]));
                            for (int k = radioRange - 1; k > 0; --k) {
                                HashSet<Cell> temp = new HashSet<>();
                                for (Cell c : neighbourlist.get(i).get(j)) {
                                    if (c != null)
                                        temp.addAll(getNeighbours(c));
                                }
                                neighbourlist.get(i).get(j).addAll(temp);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.print("sdfsdfs");
            throw e;
        }
    }

    public Cell getCell(int x, int y) throws MapException {
        if (map[x][y] == null) throw new MapException("tried getting a cell in a null spot");
        else if (x > getHeight() || y > getWidth() || x <= 0 || y <= 0)
            throw new MapException("coordinates out of bounds");
        else if (x % 2 == y % 2) return map[x][y];
        else throw new MapException("tried getting invalid coordinates");
    }


    public ArrayList<Cell> getRing2(Cell c,int range){
        Cell cc=map[c.getX()][c.getY()];
        HashSet<Cell> circle=new HashSet<>();
        if(range==0) {
            circle.add(cc);
        }else{
            ArrayList<Cell> smaller=getRing2(cc,range-1);
            for(int i=0;i<smaller.size();++i){
                circle.addAll(getNeighbours(smaller.get(i)));
            }
            circle.removeAll(smaller);
            if(range>1){
                circle.removeAll(getRing2(cc,range-2));
            }
        }
        ArrayList<Cell> a=new ArrayList<>();
        a.addAll(circle);
        return a;
    }


    public ArrayList<Cell> getRing(Cell c, int range) {
        Cell cc = map[c.getX()][c.getY()];
        Set<Cell> hs = new HashSet<>();
        ArrayList<Cell> ring = new ArrayList<>();
        ring.add(cc);

        for (int i = 1; i <= range; i++) {
            hs.clear();
            for (int j = 0; j < ring.size(); j++) {
                hs.addAll(getNeighbours(ring.get(j))); // add all the neighbours
            }
            hs.removeAll(ring); // we romove what was inside
            if (i == range) ring.clear();
            ring.addAll(hs); // we got the outter ring
        }
        return ring;
    }

    public int getDistance(Cell a, Cell b) {
        int rightDiff = (getWidth() + b.getY() - a.getY()) % getWidth();
        int leftDiff = (getWidth() + a.getY() - b.getY()) % getWidth();
        int upDiff = (getHeight() + a.getX() - b.getX()) % getHeight();
        int downDiff = (getHeight() + b.getX() - a.getX()) % getHeight();

        int distY = Math.min(rightDiff, leftDiff);
        int distX = Math.min(upDiff, downDiff);

        int distance = distY + Math.max(0, (distX - distY) / 2);
        return distance;
    }

    // [name] proudly presents his WORKING non-mathematical distance calculation function
    // back-up distance calculation
    public int getDistanceBroughtToYouByOle(Cell a, Cell b) {
        if (a.getX() == b.getX() && a.getY() == b.getY()) return 0;
        ArrayList<Cell> inRange;
        int i = 1;
        while (true) {
            inRange = new ArrayList<Cell>();
            for (int j = -i; j <= i; j += 2) {
                int y = ((((a.getY() + i - 1) % (getWidth())) + getWidth()) % getWidth()) + 1;
                int y2 = ((((a.getY() - i - 1) % (getWidth())) + getWidth()) % getWidth()) + 1;
                int x = ((((a.getX() + j - 1) % (getHeight())) + getHeight()) % getHeight()) + 1;
                if (!inRange.contains(map[x][y])) inRange.add(map[x][y]);
                if (!inRange.contains(map[x][y2])) inRange.add(map[x][y2]);
            }
            for (int jj = 0; jj < i; jj++) {
                int x = ((((a.getX() + (2 * i - jj) - 1) % (getHeight())) + getHeight()) % getHeight()) + 1;
                int x2 = ((((a.getX() - (2 * i - jj) - 1) % (getHeight())) + getHeight()) % getHeight()) + 1;
                int y = ((((a.getY() + jj - 1) % (getWidth())) + getWidth()) % getWidth()) + 1;
                int y2 = ((((a.getY() - jj - 1) % (getWidth())) + getWidth()) % getWidth()) + 1;
                if (!inRange.contains(map[x][y])) inRange.add(map[x][y]);
                if (!inRange.contains(map[x][y2])) inRange.add(map[x][y2]);
                if (!inRange.contains(map[x2][y])) inRange.add(map[x2][y]);
                if (!inRange.contains(map[x2][y2])) inRange.add(map[x2][y2]);
            }
            if (inRange.contains(b)) return i;
            i++;
        }
    }

    public int getWidth() {
        return map[0].length - 1;
    }

    public int getHeight() {
        return map.length - 1;
    }

    public void putMineral(int x, int y, String m) throws MapException {
        if (map[x][y] == null) throw new MapException("tried putting a mineral in a null spot");
        if (x % 2 == y % 2)
            map[x][y].setMineral(m);
        else throw new MapException("tried putting a mineral on invalid coordinates");
    }

    public org.xploration.ontology.Map getOntologyMap() {
        org.xploration.ontology.Map ontologyMap = new org.xploration.ontology.Map();
        for (int i = 1; i <= getHeight(); i++) {
            for (int j = 1; j <= getWidth(); j++) {
                if (isValidPosition(i, j)) {
                    Cell c = map[i][j];
                    if (c.getMineral() != null && !c.getMineral().isEmpty())
                        ontologyMap.addCellList(c);
                }
            }
        }
        return ontologyMap;
    }

    /**
     * method that reads and parses the map from a file
     *
     * @param path path of the file
     */
    private void readMapFromFile(String path) throws FileNotFoundException {

        Scanner scanner = new Scanner(new File(path));
        String line = scanner.nextLine();
        int commaIndex = line.indexOf(",");
        int x = Integer.parseInt(line.substring(1, commaIndex));
        int y = Integer.parseInt(line.substring(commaIndex + 1, line.length() - 1));
        map = new Cell[x + 1][y + 1];

        ArrayList<String[]> tempmap = new ArrayList<>();

        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            String[] m = line.split("\\s+");
            //String [] m =line.split("");
            if (m.length > 1) {
                tempmap.add(m);
            }
        }

        for (int i = 0; i < tempmap.size(); i++) {
            for (int j = 0; j < tempmap.get(i).length; ++j) {
                int xx = i + 1;
                int yy;
                if (xx % 2 == 1) {
                    yy = (2 * j) + 1;
                } else {
                    yy = 2 * (j + 1);
                }
                Cell c = new Cell();
                c.setX(xx);
                c.setY(yy);
                c.setMineral(tempmap.get(i)[j]);
                map[xx][yy] = c;
            }
        }
    }

    public boolean isValidCell(Cell c) {
        return isValidPosition(c.getX(), c.getY());
    }

    public boolean isValidPosition(int x, int y) {
        if (x == 0 || y == 0) {
            return false;
        } else if (x > getHeight() || y > getWidth()) {
            return false;
        } else {
            return x % 2 == y % 2;
        }
    }

    public ArrayList<Cell> getNeighbours(Cell c) {
        int x = c.getX() - 1;
        int y = c.getY() - 1;
        int xx;
        int yy;
        ArrayList<Cell> nbs = new ArrayList<>();
        //up
        xx = (((x - 2) % getHeight()) + getHeight()) % getHeight();
        yy = y;
        nbs.add(map[xx + 1][yy + 1]);
        //up right
        xx = (((x - 1) % getHeight()) + getHeight()) % getHeight();
        yy = (((y + 1) % getWidth()) + getWidth()) % getWidth();
        nbs.add(map[xx + 1][yy + 1]);
        //down right
        xx = (((x + 1) % getHeight()) + getHeight()) % getHeight();
        yy = (((y + 1) % getWidth()) + getWidth()) % getWidth();
        nbs.add(map[xx + 1][yy + 1]);
        //down
        xx = (((x + 2) % getHeight()) + getHeight()) % getHeight();
        yy = y;
        nbs.add(map[xx + 1][yy + 1]);
        //down left
        xx = (((x + 1) % getHeight()) + getHeight()) % getHeight();
        yy = (((y - 1) % getWidth()) + getWidth()) % getWidth();
        nbs.add(map[xx + 1][yy + 1]);
        //up left
        xx = (((x - 1) % getHeight()) + getHeight()) % getHeight();
        yy = (((y - 1) % getWidth()) + getWidth()) % getWidth();
        nbs.add(map[xx + 1][yy + 1]);
        return nbs;
    }

    public HashSet<Cell> getInCommunicationsRange(Cell c) {
        return neighbourlist.get(c.getX()).get(c.getY());
    }
}